import 'package:flutter/material.dart';
import 'package:mobilemidterm_project/homePage.dart';


class LoginPage extends StatelessWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: null,
        body: Center(
          child: ConstrainedBox(
            constraints: const BoxConstraints(maxWidth: 300.0),
            child: Column(
              children: [

                const Spacer(flex: 5),
                Image.network("https://upload.wikimedia.org/wikipedia/commons/e/ec/Buu-logo11.png"
                  , width: 150,),

                const Spacer(flex: 7),
                const TextField(
                    decoration: InputDecoration(labelText: 'Username')),
                const TextField(
                    decoration: InputDecoration(labelText: 'Password')),
                const Spacer(flex: 5),



                ElevatedButton(onPressed: () {
                  Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) {
                    return homePage();
                  }));
                }, child: const Text('เข้าสู่ระบบ')),
                const Spacer(flex: 20),
              ],
            ),
          ),
        )
    );
  }
}
