import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobilemidterm_project/gradeResultPage.dart';
import 'package:mobilemidterm_project/homePage.dart';
import 'package:mobilemidterm_project/loginPage.dart';
import 'package:mobilemidterm_project/profilePage.dart';




class scheduleTable extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: Text("Burapha University",),
        backgroundColor: Colors.yellow.shade300,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) {
                    return LoginPage();
                  }));
            },
            icon: Icon(
              Icons.logout,
              color: Colors.black,
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        height: 75,
        child: Container(
          width: double.infinity,
          color: Colors.yellow.shade300,
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                            return profilePage();
                          }));
                    },
                    icon: Icon(Icons.person, color: Colors.black38),
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                            return scheduleTable();
                          }));
                    },
                    icon: Icon(Icons.library_books, color: Colors.black38),
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                            return gradeResultPage();
                          }));
                    },
                    icon: Icon(Icons.book, color: Colors.black38),
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                            return homePage();
                          }));
                    },
                    icon: Icon(Icons.home, color: Colors.black38),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
      backgroundColor: Colors.white70,
      body: ListView(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("Schedule Table", style: TextStyle(fontSize: 24.0),
                        ),
                        Text(""),
                      ],
                    ),
                    Container(
                      height: 50,
                      width: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            color: Colors.yellow.shade300,
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("Mon"),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("Tue"),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("Wed"),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("Thu"),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("Fri"),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("Sat"),
                        Text(""),
                      ],
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(""),
                        Text("Sun"),
                        Text(""),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          firstSchedule(),
          secondSchedule(),
          thirdSchedule(),
        ],
      ),
    );
  }
}

class Line extends StatelessWidget {
  final lines;
  const Line({
    Key? key,
    this.lines,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: List.generate(
        4,
            (index) => Container(
          height: 2.0,
          width: lines[index],
          color: Color(0xffd0d2d8),
          margin: EdgeInsets.symmetric(vertical: 14.0),
        ),
      ),
    );
  }
}

class firstSchedule extends StatelessWidget {
  const firstSchedule({Key? key,})
      :super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Line(
                lines: [0.0, 0.0, 0.0, 4.0],
              ),
            ],
          ),
        ),
        SizedBox(
          width: 10.0,
        ),
        Expanded(
          child: Container(
            height: 100.0,
            child: Container(
              margin: EdgeInsets.only(left: 4.0),
              color: Colors.yellow.shade300,
              padding: EdgeInsets.only(left: 16.0, top: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 21.0,
                    child: Row(
                      children: <Widget>[
                        Text("10:00-12:00"),
                        VerticalDivider(),
                        Text("88634459-59"),
                        VerticalDivider(),
                        Text("IF-4C02, IF"),
                      ],
                    ),
                  ),
                  Text(""),
                  Text(
                    "Mobile Application Development I",
                    style:
                    TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class secondSchedule extends StatelessWidget {
  const secondSchedule({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Line(
                lines: [0.0, 0.0, 0.0, 4.0],
              ),
            ],
          ),
        ),
        SizedBox(
          width: 12.0,
        ),
        Expanded(
          child: Container(
            height: 130.0,
            child: Container(
              margin: EdgeInsets.only(left: 4.0),
              color: Colors.yellow.shade300,
              padding: EdgeInsets.only(left: 16.0, top: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 21.0,
                    child: Row(
                      children: <Widget>[
                        Text("13:00 - 15:00"),
                        VerticalDivider(),
                        Text("88634259-59"),
                        VerticalDivider(),
                        Text("IF-4C01, IF"),
                      ],
                    ),
                  ),
                  Text(""),
                  Text(
                    "Multimedia Programming for Multiplatforms",
                    style:
                    TextStyle(fontSize: 20.0),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}

class thirdSchedule extends StatelessWidget {
  const thirdSchedule({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: <Widget>[
              Line(
                lines: [0.0, 0.0, 0.0, 4.0],
              ),
            ],
          ),
        ),
        SizedBox(
          width: 12.0,
        ),
        Expanded(
          child: Container(
            height: 100.0,
            child: Container(
              margin: EdgeInsets.only(left: 4.0),
              color: Colors.yellow.shade300,
              padding: EdgeInsets.only(left: 16.0, top: 8.0),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    height: 21.0,
                    child: Row(
                      children: <Widget>[
                        Text("15:00 - 17:00"),
                        VerticalDivider(),
                        Text("88624359-59"),
                        VerticalDivider(),
                        Text("IF-3C01, IF"),
                      ],
                    ),
                  ),
                  Text(""),
                  Text(
                    "	Web Programming",
                    style:
                    TextStyle(fontSize: 20),
                  ),
                ],
              ),
            ),
          ),
        )
      ],
    );
  }
}