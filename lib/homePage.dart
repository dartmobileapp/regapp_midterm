import 'package:flutter/material.dart';
import 'package:mobilemidterm_project/gradeResultPage.dart';
import 'package:mobilemidterm_project/loginPage.dart';
import 'package:mobilemidterm_project/profilePage.dart';
import 'package:mobilemidterm_project/scheduleTable.dart';


class homePage extends StatelessWidget {
  const homePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Burapha University",
        ),
        backgroundColor: Colors.yellow.shade300,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) {
                    return LoginPage();
                  }));
            },
            icon: Icon(
              Icons.logout,
              color: Colors.black,
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
          height: 75,
          child: Container(
            width: double.infinity,
            color: Colors.yellow.shade300,
            child: ListView(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) {
                              return profilePage();
                            }));
                      },
                      icon: Icon(Icons.person, color: Colors.black38),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) {
                              return scheduleTable();
                            }));
                      },
                      icon: Icon(Icons.library_books, color: Colors.black38),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) {
                              return gradeResultPage();
                            }));
                      },
                      icon: Icon(Icons.book, color: Colors.black38),
                    ),
                    IconButton(
                      onPressed: () {

                      },
                      icon: Icon(Icons.home,
                          color: Colors.black38),
                    ),
                  ],
                )
              ],
            ),
          )),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/281152851_5910050635677276_1323122901884701339_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=H4IUTTAn07AAX88N3aL&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQVJk8npvzUH1leIAcSFeIDo-AkkTGmuB-k64Y_wKFhvQ&oe=63F49539"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(8.0),
        child: ListView(
          children: <Widget>[
            news1(),
            news2(),
            news3(),
          ],
        ),
      ),
    );
  }
}

Widget news1() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(5.0)),
    color: Colors.white,
    child: SizedBox(
      height: 130,
      child: ListView(
        padding: EdgeInsets.all(8.0),
        children: <Widget>[
          infoNews1(),
        ],
      ),
    ),
  );
}

Widget infoNews1() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("                                  ประกาศเรื่อง",
          style: TextStyle(
              height: 2.0, color: Colors.amberAccent.shade700, fontSize: 15)),
      Padding(
        padding: EdgeInsets.only(right: 30),
        child: Text("แบบประเมินความเห็นของนิสิตต่อ"
            "การบริการของสำนักงานอธิการ",style: TextStyle(height: 3.5, fontSize: 12)),
      ),
      urlNews1(),
    ],
  );
}

Widget urlNews1() {
  return Column(
    children: [
      Padding(
        padding: EdgeInsets.only(top: 5, left: 0),
        child: Text("ประเมินความคิดเห็นที่ https://bit.ly/3cyvuuf"),
      )
    ],
  );
}

Widget news2() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: 440,
      child: ListView(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.all(8.0),
        children: <Widget>[
          infoNews2(),
        ],
      ),
    ),
  );
}

Widget infoNews2() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("                                  ประกาศเรื่อง",
          style: TextStyle(
              height: 2.0, color: Colors.amberAccent.shade700, fontSize: 15)),
      Padding(
        padding: EdgeInsets.only(right: 30),
        child: Text("การทำบัตรนิสิตกับธนาคารกรุงไทย",
            style: TextStyle(height: 3.5, fontSize: 12)),
      ),
      urlNews2(),
    ],
  );
}

Widget urlNews2() {
  return Column(
    children: [
      Padding(
          padding: EdgeInsets.all(8),
          child: Image.network(
              'https://scontent.fbkk5-4.fna.fbcdn.net/v/t1.15752-9/326323529_859258131997513_5781641427720499130_n.png?_nc_cat=110&ccb=1-7&_nc_sid=ae9488&_nc_ohc=0Brd7ALe-AEAX8Cy2pY&tn=ee5827Vjg-kg1k7B&_nc_ht=scontent.fbkk5-4.fna&oh=03_AdTG85HJt8AMHO7JyIqtNeuUrJEqgupzi9OjPisnthulzA&oe=63F4C776',
              height: 330)),
    ],
  );
}

Widget news3() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: 300,
      child: ListView(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.all(8.0),
        children: <Widget>[
          infoNews3(),
        ],
      ),
    ),
  );
}

Widget infoNews3() {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    children: <Widget>[
      Text("                                  ประกาศเรื่อง",
          style: TextStyle(
              height: 2.0, color: Colors.amberAccent.shade700, fontSize: 15)),
      Padding(
        padding: EdgeInsets.only(right: 30),
        child: Text("ประกาศรายชื่อผู้มีสิทธิ์สอบสัมภาษณ์ TCAS รอบ 1",
            style: TextStyle(height: 2.0, fontSize: 12)),
      ),
      urlNews3(),
    ],
  );
}

Widget urlNews3() {
  return Column(
    children: [
      Padding(
          padding: EdgeInsets.all(0),
          child: Image.network(
              'https://scontent.fbkk30-1.fna.fbcdn.net/v/t39.30808-6/327398674_877863356602347_6491656780640545138_n.jpg?_nc_cat=100&ccb=1-7&_nc_sid=730e14&_nc_eui2=AeEtbfsVk1esQH7KVOZbTXo7qiGm41Uf4BWqIabjVR_gFR3W6-88T_6tYLm25OOFaSCYz-ft6JfbN3HM4L-ID8Vx&_nc_ohc=Hj_UFHKALMoAX8KQ2Xo&_nc_ht=scontent.fbkk30-1.fna&oh=00_AfBP9duSBuPMTLLgQpFKTtt82mSkdmUnhGrv__HRp3vzqA&oe=63DE47BF',
              height: 180)),
    ],
  );
}