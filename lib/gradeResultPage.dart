import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mobilemidterm_project/homePage.dart';
import 'package:mobilemidterm_project/loginPage.dart';
import 'package:mobilemidterm_project/profilePage.dart';
import 'package:mobilemidterm_project/scheduleTable.dart';

class gradeResultPage extends StatelessWidget {
  const gradeResultPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Grade Results",
        ),
        backgroundColor: Colors.yellow.shade300,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) {
                return LoginPage();
              }));
            },
            icon: Icon(
              Icons.logout,
              color: Colors.black,
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomAppBar(
        height: 75,
        child: Container(
          width: double.infinity,
          color: Colors.yellow.shade300,
          child: ListView(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  IconButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                        return profilePage();
                      }));
                    },
                    icon: Icon(Icons.person, color: Colors.black38),
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                        return scheduleTable();
                      }));
                    },
                    icon: Icon(Icons.library_books, color: Colors.black38),
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                        return gradeResultPage();
                      }));
                    },
                    icon: Icon(Icons.book, color: Colors.black38),
                  ),
                  IconButton(
                    onPressed: () {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (context) {
                        return homePage();
                      }));
                    },
                    icon: Icon(Icons.home, color: Colors.black38),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
      body: Container(
        color: Colors.white,
        child: ListView(
          children: [
            Container(
              width: 30,
              height: 70,
              child: Center(
                child: Container(
                  child: Text(
                    'ภาคการศึกษาที่ 1/2563',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.yellow.shade300,
              height: 350,
              width: 280,
              child: DataTable(
                columns: [
                  DataColumn(
                    label: Text(
                      'รายวิชา',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'หน่วยกิต',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'เกรด',
                      style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
                rows: [
                  DataRow(
                    cells: [
                      DataCell(Text('Art and Life')),
                      DataCell(Text('3')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Arts and Creativity')),
                      DataCell(Text('2')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Mathematics for Computing')),
                      DataCell(Text('3')),
                      DataCell(Text('B')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Programming Fundamental')),
                      DataCell(Text('3')),
                      DataCell(Text('B+')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Introduction to Computer Science')),
                      DataCell(Text('3')),
                      DataCell(Text('B+')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('English Writing for Communication')),
                      DataCell(Text('3')),
                      DataCell(Text('A')),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 200,
              height: 70,
              margin: EdgeInsets.all(10),
              decoration: ShapeDecoration(
                color: Colors.blueGrey,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Center(
                              child: Container(
                                child: Text(
                                  'This semester GPA 3.65',
                                  style: TextStyle(
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Center(
                              child: Container(
                                child: Text(
                                  '(C.Register 17 | C.Earn 17)',
                                  style: TextStyle(
                                      fontSize: 18, color: Colors.white),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 30,
              height: 70,
              child: Center(
                child: Container(
                  child: Text(
                    'ภาคการศึกษาที่ 2/2563',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.yellow.shade300,
              height: 350,
              width: 280,
              child: DataTable(
                columns: [
                  DataColumn(
                    label: Text(
                      'รายวิชา',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'หน่วยกิต',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'เกรด',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
                rows: [
                  DataRow(
                    cells: [
                      DataCell(Text('Marine Ecology')),
                      DataCell(Text('2')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Sufficiency Economy')),
                      DataCell(Text('2')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Exercise for Quality')),
                      DataCell(Text('2')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Logical Thinking')),
                      DataCell(Text('3')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Digital Society with ICT')),
                      DataCell(Text('3')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Discrete Structures')),
                      DataCell(Text('3')),
                      DataCell(Text('C+')),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 200,
              height: 70,
              margin: EdgeInsets.all(10),
              decoration: ShapeDecoration(
                color: Colors.blueGrey,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Center(
                              child: Container(
                                child: Text(
                                  'This semester GPA 3.59',
                                  style: TextStyle(
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Center(
                              child: Container(
                                child: Text(
                                  '(C.Register 15 | C.Earn 32)',
                                  style: TextStyle(
                                      fontSize: 18, color: Colors.white),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 30,
              height: 70,
              child: Center(
                child: Container(
                  child: Text(
                    'ภาคการศึกษาที่ 1/2564',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.yellow.shade300,
              height: 350,
              width: 280,
              child: DataTable(
                columns: [
                  DataColumn(
                    label: Text(
                      'รายวิชา',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'หน่วยกิต',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'เกรด',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
                rows: [
                  DataRow(
                    cells: [
                      DataCell(Text('Economics of Everyday Life')),
                      DataCell(Text('2')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Calculus')),
                      DataCell(Text('3')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Probability and Statistics for Computing')),
                      DataCell(Text('3')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('English for Informatics')),
                      DataCell(Text('3')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Object-Oriented Programming Paradigm')),
                      DataCell(Text('3')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Relational Database')),
                      DataCell(Text('3')),
                      DataCell(Text('B')),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 200,
              height: 70,
              margin: EdgeInsets.all(10),
              decoration: ShapeDecoration(
                color: Colors.blueGrey,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Center(
                              child: Container(
                                child: Text(
                                  'This semester GPA 3.75',
                                  style: TextStyle(
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Center(
                              child: Container(
                                child: Text(
                                  '(C.Register 17 | C.Earn 49)',
                                  style: TextStyle(
                                      fontSize: 18, color: Colors.white),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 30,
              height: 70,
              child: Center(
                child: Container(
                  child: Text(
                    'ภาคการศึกษาที่ 2/2564',
                    style: TextStyle(
                      fontSize: 20,
                    ),
                  ),
                ),
              ),
            ),
            Container(
              color: Colors.yellow.shade300,
              height: 350,
              width: 280,
              child: DataTable(
                columns: [
                  DataColumn(
                    label: Text(
                      'รายวิชา',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'หน่วยกิต',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                  DataColumn(
                    label: Text(
                      'เกรด',
                      style:
                      TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                    ),
                  ),
                ],
                rows: [
                  DataRow(
                    cells: [
                      DataCell(Text('Adolescent Health')),
                      DataCell(Text('2')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Non-Relational Database')),
                      DataCell(Text('3')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Introduction to Data Science and Data Analytics')),
                      DataCell(Text('3')),
                      DataCell(Text('B')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Data Structures and Algorithms')),
                      DataCell(Text('3')),
                      DataCell(Text('A')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Operating Systems')),
                      DataCell(Text('3')),
                      DataCell(Text('C+')),
                    ],
                  ),
                  DataRow(
                    cells: [
                      DataCell(Text('Unix Tools and Programming')),
                      DataCell(Text('3')),
                      DataCell(Text('C')),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 200,
              height: 70,
              margin: EdgeInsets.all(10),
              decoration: ShapeDecoration(
                color: Colors.blueGrey,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20),
                ),
              ),
              child: ListView(
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            margin: EdgeInsets.only(top: 10),
                            child: Center(
                              child: Container(
                                child: Text(
                                  'This semester GPA 3.24',
                                  style: TextStyle(
                                    fontSize: 20,
                                  ),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Row(
                        children: [
                          Container(
                            child: Center(
                              child: Container(
                                child: Text(
                                  '(C.Register 17 | C.Earn 66)',
                                  style: TextStyle(
                                      fontSize: 18, color: Colors.white),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ],
                  ),
                ],
              ),
            ),
            Container(
              width: 350,
              height: 100,
              margin: EdgeInsets.all(15),
              decoration: ShapeDecoration(
                color: Colors.blueGrey.shade50,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(12),
                ),
              ),
              child: Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      child: Text(
                        'Cumulative this semester GPA',
                        style:
                        TextStyle(fontSize: 18, color: Colors.black),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Text(
                            '3.54',
                            style: TextStyle(
                                fontSize: 30, color: Colors.black),
                          ),
                        )
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Container(
                          child: Text(
                            '(C.Register 66 | C.Earn 66)',
                            style: TextStyle(
                                fontSize: 18, color: Colors.black),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
