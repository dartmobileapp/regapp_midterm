import 'package:flutter/material.dart';
import 'package:mobilemidterm_project/gradeResultPage.dart';
import 'package:mobilemidterm_project/homePage.dart';
import 'package:mobilemidterm_project/loginPage.dart';
import 'package:mobilemidterm_project/scheduleTable.dart';


void main() {
  runApp(const profilePage());
}

class profilePage extends StatelessWidget {
  const profilePage({Key? key}) : super(key: key);

  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title:
        Text("Profile",),
        backgroundColor: Colors.yellow.shade300,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) {
                    return LoginPage();
                  }));
            },
            icon: Icon(
              Icons.logout,
              color: Colors.black,
            ),
          ),
        ],
      ),
      extendBodyBehindAppBar: true,
      bottomNavigationBar: BottomAppBar(
          height: 75,
          child: Container(
            width: double.infinity,
            color: Colors.yellow.shade300,
            child: ListView(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    IconButton(
                      onPressed: () {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) {
                              return profilePage();
                            }));
                      },
                      icon: Icon(Icons.person, color: Colors.black38),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) {
                              return scheduleTable();
                            }));
                      },
                      icon: Icon(Icons.library_books, color: Colors.black38),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) {
                              return gradeResultPage();
                            }));
                      },
                      icon: Icon(Icons.book, color: Colors.black38),
                    ),
                    IconButton(
                      onPressed: () {
                        Navigator.pushReplacement(context,
                            MaterialPageRoute(builder: (context) {
                              return homePage();
                            }));
                      },
                      icon: Icon(Icons.home,
                          color: Colors.black38),
                    ),
                  ],
                )
              ],
            ),
          )),
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
                "https://scontent.fbkk5-7.fna.fbcdn.net/v/t1.15752-9/281152851_5910050635677276_1323122901884701339_n.png?_nc_cat=107&ccb=1-7&_nc_sid=ae9488&_nc_ohc=H4IUTTAn07AAX88N3aL&_nc_ht=scontent.fbkk5-7.fna&oh=03_AdQVJk8npvzUH1leIAcSFeIDo-AkkTGmuB-k64Y_wKFhvQ&oe=63F49539"),
            fit: BoxFit.cover,
          ),
        ),
        padding: EdgeInsets.all(15.0),
        child: ListView(
          children: <Widget>[
            PictureProfile(),
            Biography(),
          ],
        ),
      ),
    );
  }
}

Widget PictureProfile(){
  return Row(
    mainAxisAlignment: MainAxisAlignment.center,
    children: [
      Column(
          children: [
            Image.network(
              "https://upload.wikimedia.org/wikipedia/commons/thumb/9/92/Jin_at_Melon_Music_Awards%2C_14_November_2013.jpg/340px-Jin_at_Melon_Music_Awards%2C_14_November_2013.jpg",
              fit: BoxFit.cover,
            ),
          ]
      ),
    ],
  );
}

Widget Biography() {
  return Card(
    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
    color: Colors.white,
    child: SizedBox(
      height: 260,
      child: ListView(
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.all(15.0),
        children: <Widget>[
          ContextBiography(),
          DataBiography(),
        ],
      ),
    ),
  );
}

Widget ContextBiography() {
  return Row(
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("   ประวัตินิสิต",
              style: TextStyle(
                  height: 1.5,
                  color: Colors.amberAccent.shade700,
                  fontSize: 15)),
          Text("   รหัสประจำตัวนิสิต :", style: TextStyle(height: 2.8, fontSize: 13)),
          Text("   ชื่อ-สกุล :", style: TextStyle(height: 2.0, fontSize: 13)),
          Text("   คณะ :", style: TextStyle(height: 2.0, fontSize: 13)),
          Text("   สาขา :", style: TextStyle(height: 1.5, fontSize: 13)),
          Text("   สถานะ :", style: TextStyle(height: 2.0, fontSize: 13)),
          Text("   อาจารย์ที่ปรึกษา :", style: TextStyle(height: 2.0, fontSize: 13)),
        ],
      ),
    ],
  );
}

Widget DataBiography() {
  return Row(
    children: <Widget>[
      Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text("", style: TextStyle(height: 2.0, fontSize: 15)),
          Text(" 63160078", style: TextStyle(height: 2.7, fontSize: 13)),
          Text("นายพีระยุทธ พรมนอก", style: TextStyle(height: 2.0, fontSize: 13)),
          Text("วิทยาการสารสนเทศ", style: TextStyle(height: 2.0, fontSize: 13)),
          Text("วิทยาการคอมพิวเตอร์", style: TextStyle(height: 2.0, fontSize: 13)),
          Text("กำลังศึกษาอยู่", style: TextStyle(height: 2.0, fontSize: 13)),
          Text("อาจารย์ภูษิต กุลเกษม, ", style: TextStyle(height: 2, fontSize: 13)),
          Text("ผู้ช่วยศาสตราจารย์ ดร.โกเมษ อัมพวัน", style: TextStyle(height: 2.0, fontSize: 13)),
        ],
      ),
    ],
  );
}






